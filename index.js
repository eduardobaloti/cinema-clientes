const express = require('express');
const app = express();
app.use(express.json());
const axios = require("axios");
const clientes = {};
contador = 0;
app.get('/clientes', (req, res) => {
    res.send(clientes);
});
app.put('/clientes', async (req, res) => {
    contador++;
    const {
        texto
    } = req.body;
    clientes[contador] = {
        contador,
        texto
    }
    await axios.post("http://localhost:10000/eventos", {
        tipo: "ClienteCriado",
        dados: {
            contador,
            texto,
        },
    });
    res.status(201).send(clientes[contador]);
});
app.listen(4000, () => {
    console.log('cliente. Porta 4000');
});